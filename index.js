let http = require('http')

let port =  4000

let server = http.createServer(function(request,response){

	if(request.url == '/' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to booking system')
		} else if (request.url == '/profile') {
			response.writeHead(200, {'Content-Type':'text/plain'})
			response.end('Welcome to your profile')
		} else if(request.url == '/courses'){
			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end("Here's our courses available")
		}
	if(request.url == '/addCourse' && request.method == 'POST'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('add course to our resources')
	}
})



server.listen(port)

console.log(`Server now accessible at localhost:${port}`)